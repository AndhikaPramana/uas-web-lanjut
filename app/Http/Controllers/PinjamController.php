<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pinjam;
// use App\Models\akun;
use App\Models\zoom;
use Illuminate\Routing\Controller;

class PinjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pinjamakun = zoom::where('status_aktif','Aktif')->get();
        // return ($pinjamakun);
        return view('user.tampil_peminjaman', ['pinjamakun'=>$pinjamakun]);
    }

    function indexSecond()
    {
        $pinjam = pinjam::all();
        return view('user.post_req', 
        ['pinjam'=>$pinjam]);
    }

    public function postRequest(Request $request)
    { 
        $pinjam = new pinjam();
        $pinjam->akun_id = $request->id_zoom;
        // $pinjam->nama_akun = $request->nama_akun;
        $pinjam->nama_peminjam = $request->nama_peminjam;
        $pinjam->status_pinjam = 'Request';
        $pinjam->nama_kegiatan = $request->kegiatan;
        $pinjam->keterangan = $request->keterangan;
        $pinjam->durasi = $request->durasi;
        $pinjam->save();

        return redirect('/post_req')->with('message','Data zoom baru berhasil d tambahkan!');
    }

    public function updateKembali(Request $request, $id)
    {   
        if($request->isMethod('post')) {
            $pinjam = $request->all();
            pinjam::where(['id'=> $id])->update([
                    'tanggal_kembali'=>$pinjam['tanggal_kembali'],
                    'status_pinjam'=>$pinjam['status_pinjam'],
            ]);
            return redirect()->back()->with('bubububu','Akun Telah di Kembalikan!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataZoom = pinjam::find($id);
        $dataZoom->delete();
        return redirect()->back()->with('mes','data berhasil terhapus');
    }
}
