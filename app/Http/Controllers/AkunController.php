<?php

namespace App\Http\Controllers;

use App\Models\akun;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class C_Akun extends Controller
{
    public function index()
    {
        $akun = akun::all();
        return view('admin.akun',['akun' => $akun]);
    }

    public function postInsert(Request $request)
    { 
        $request->validate([
            'kapasitas' => 'required|min:3',
        ]);
        $akun = new akun;
        $akun->name = $request->name;
        $akun->status = 'aktif';
        $akun->kapasitas = $request->kapasitas;
        $akun->save();

        return redirect('/akun')->with('message','Data zoom baru berhasil d tambahkan!');
    }


    public function postUpdate(Request $request, $id)
    {
        $request->validate([
            'kapasitas' => 'required|min:3',
        ]);
        
        if($request->isMethod('post')) {
            $data = $request->all();
            akun::where(['id'=> $id])->update([
                    'name'=>$data['name'],
                    'kapasitas'=>$data['kapasitas'],
                    'status'=>$data['status'],
            ]);
            return redirect()->back()->with('pesuang','Data Berhasil di Ubah!');
        }
    }

    public function delete($id) {
        $akun = akun::find($id);
        $akun->delete();
        return redirect('/akun')->with('pesuandel','Data Berhasil di Hapus Secara Permanent!');
    }
}
