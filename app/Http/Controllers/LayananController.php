<?php

namespace App\Http\Controllers;

use App\Models\pinjam;
use Illuminate\Http\Request;


class LayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataZoom = pinjam::where('status_pinjam', '!=', 'Dikembalikan')->get();
        return view('admin.layanan', ['dataZoom' => $dataZoom]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $dataZoom = $request->all();
            pinjam::where(['id' => $id])->update([
                'status_pinjam' => $dataZoom['status_pinjam'],
            ]);
            return redirect()->back()->with('aprove', 'Peminjaman Telah di Aprove!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $dataZoom = pinjam::find($id);
        $dataZoom->delete();
        return redirect('/layanan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleter($id)
    {
        $dataZoom = pinjam::find($id);
        $dataZoom->delete();
        return redirect()->back()->with('messgaeERR','data berhasil terhapus');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, $id)
    {

        if ($request->isMethod('post')) {
            $dataZoom = $request->all();
            pinjam::where(['id' => $id])->update([
                'status_pinjam' => $dataZoom['status_pinjam'],
            ]);
            return redirect()->back()->with('reject', 'Permintaan Akun Berhasil di Reject!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataZoom = pinjam::find($id);
        if ($dataZoom != null) {
            $dataZoom->delete();
            return redirect('/layanan');
        }
    }
}
