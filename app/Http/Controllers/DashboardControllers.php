<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class DashboardControllers extends Controller
{
    function index()
    {
        return view('dashboard');
    }

    public function admin()
    {
        return view('halaman.admin');
    }

    public function user()
    {
        return view('halaman.user');
    }



    public function manage_jadwal()
    {
        return view('admin.manage_jadwal');
    }

    public function manage_peminjaman()
    {
        return view('admin.manage_peminjaman');
    }

    public function ajukan_peminjaman()
    {
        return view('user.ajukan_peminjaman');
    }


    public function daftar_akun()
    {
        return view('daftar_akun');
    }
}
