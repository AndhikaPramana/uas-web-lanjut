<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\zoom;

class ZoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function zoomlist()
    {
        return view('zoom.zoomlist');
    }


    public function index()
    {
        $dataZoom = zoom::all(); // menampilkan data tampa data yang di soft delete
        //$zoom = zoom::withTrashed()->get(); // menampilkan data yang sudah di soft delete
        return view('zoom.zoomlist', ['dataZoom' => $dataZoom]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('zoom.zoomcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kapasitas' => 'required|min:3',
            'nama_akun' => 'required'
        ]);
        $dataZoom = new zoom();
        $dataZoom->nama_akun = $request->nama_akun;
        $dataZoom->kapasitas = $request->kapasitas;
        $dataZoom->status_aktif = 'aktif';

        $dataZoom->save();

        return redirect('/listzoom');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataZoom = zoom::where('id', $id)->first();
        $dataZoom = ['dataZoom' => $dataZoom];
        return view('zoom.zoomedit', $dataZoom);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataZoom = zoom::find($id);
        $dataZoom->nama_akun = $request->nama_akun;
        $dataZoom->kapasitas = $request->kapasitas;
        $dataZoom->status_aktif = $request->status_aktif;

        $dataZoom->save();

        return redirect('/listzoom');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataZoom = zoom::find($id);
        if ($dataZoom != null) {
            $dataZoom->delete();
            return redirect('/listzoom');
        }
    }
}
