<?php

namespace App\Http\Controllers;

use App\Mail\ForgetPasswordMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Facade\FlareClient\View;
use Illuminate\Routing\Controller;
use App\Models\User;
use Illuminate\Support\Str;
use App\Models\PasswordReset;
use Carbon\Carbon;

class LoginControllers extends Controller
{
    public function login()
    {
        return view('pengguna.login');
    }

    public function postlogin(Request $request)
    {
        // dd($request->all());

        if (Auth::attempt($request->only('email', 'password'))) {
            return redirect('/dashboard');
        }
        return redirect('login')->with('errorLogin', 'Incorrect username or password. Please Try Again!!');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('login');
    }

    public function registrasi()
    {
        return view('pengguna.registrasi');
    }

    public function simpanregistrasi(Request $request)
    {
        // dd($request->all());

        User::create([
            'name' => $request->name,
            'level' => $request->level,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => Str::random(60),
        ]);

        return redirect('/login')->with('successregister', 'Register successfull, Please Login Again!');
    }

    public function getforgetpassword()
    {
        return view('pengguna.forgetpassword');
    }

    public function postforgetpassword(Request $request)
    {
        $request->validate([
            'email'=>'required|email|exists:users'
        ]);
        $user=User::where('email',$request->email)->first();
        if(!$user){
            return redirect()->back()->with('errorforget','User not Found');
        }else{
            $reset_code=Str::random(50);
            PasswordReset::create([
            'user_id'=>$user->id,
            'reset_code'=>$reset_code
            ]);

            Mail::to($user->email)->send(new ForgetPasswordMail($user->name,$reset_code));

            return redirect()->back()->with('successforgot','Please check email for reset your password!');
        }
    }

    public function getResetPassword($reset_code){
        $password_reset_data=PasswordReset::where('reset_code',$reset_code)->first();

        if(!$password_reset_data || Carbon::now()->subMinutes(50)> $password_reset_data->created_at){
            return redirect()->route('getforgetpassword')->with('errorresetlink','Invalid password reset link or link expired.');
        }else{
            return view('pengguna.resetpassword', compact('reset_code'));
        }
    }

    public function postResetPassword($reset_code,Request $request){
        $password_reset_data=PasswordReset::where('reset_code',$reset_code)->first();

        if(!$password_reset_data || Carbon::now()->subMinutes(10)> $password_reset_data->created_at){
            return redirect()->route('getforgetpassword')->with('errorresetlink','Invalid password reset link or link expired.');
        }else{
            $request->validate([
                'email'=>'required|email',
                'password'=>'required|max:100',
                'confirm_password'=>'required|same:password',
            ]);

            $user=User::find($password_reset_data->user_id);

            if($user->email!=$request->email){
                return redirect()->back()->with('errorresetps','Enter correct email.');
            }else{

                $password_reset_data->delete();
                $user->update([
                    'password'=>bcrypt($request->password)
                ]);

                return redirect()->route('login')->with('succesrestps','password successfully reset, please login again!');
            }

        }
    }
}

    // public function postforgotpassword(Request $request)
    // {
    //     $request->validate([
    //         'email' => 'requires|email'
    //     ]);
    //     $user = User::where('email', $request->email)->first();

    //     if (!$user) {
    //         return redirect()->back()->with('errorforget', 'ASU');
    //     } else {
    //     }
    // }

