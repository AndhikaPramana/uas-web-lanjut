<?php

namespace App\Http\Controllers;

use App\Models\pinjam;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    function index()
    {
        $detail = pinjam::where('status_pinjam', 'Aprove')->get();
        return view('user.detail', ['detail' => $detail]);
    }
}

