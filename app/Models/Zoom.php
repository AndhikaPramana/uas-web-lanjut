<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class zoom extends Model
{
    protected $table = 'zoom';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'nama_akun',
        'kapasitas',
        'status_aktif'

    ];

    public function pinjam()
    {
        return $this->hasOne(pinjam::class);
    }
}
