<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pinjam extends Model
{
    protected $table = 'pinjam';
    protected $fillable = [
        'nama_peminjam',
        'nama_kegiatan',
        'keterangan',
        'durasi',
        'status_pinjam',
        'tanggal_kembali',
        'akun_id'
    ];

    public function zoom()
    {
        return $this->belongsTo(zoom::class, 'akun_id');
    }
}
