@extends('layouts.app')

@section('js_script')
<script>
    $('.deleting').click(function () {
        var deletinger = $(this).attr('data-id')
            swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                window.location = "/deleting/" +deletinger+ ""
                swal("Poof! Your imaginary file has been deleted!", {
                icon: "success",
                });
            } else {
                swal("Your imaginary file is safe!");
            }
        });
    });
</script>
@endsection

@section('content')
<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Daftar</strong> Request Peminjaman Akun Zoom
      </div>
      <div class="card shadow mb-4">
    
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Akun</th>
                            <th>Nama Akun</th>
                            <th>Nama Peminjam</th>
                            <th>Kapasitas</th>
                            <th>Durasi</th>
                            <th>Status Peminjaman</th>
                            <th>Tanggal Peminjaman</th>
                            <th>Tanggal Kembali</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                        @foreach ($pinjam as $list)
                        <tbody>
                            <td>{{ $loop->iteration }}</td>
                            <td class="id">{{ $list->zoom->id }}</td>
                            <td class="nama_akun">{{ $list->zoom->nama_akun }}</td>
                            <td class="nama_peminjam">{{ $list->nama_peminjam }}</td>
                            <td class="kapasitas">{{ $list->zoom->kapasitas }} Peserta</td>
                            <td class="id">{{ $list->durasi }}</td>
                            <td class="status" align="center">
                                @if ($list->status_pinjam == 'Request')
                                <label class="m-badge m-badge--focus m-badge--wide">{{ $list->status_pinjam }}</label>
                                @elseif($list->status_pinjam == 'Aprove')
                                <label class="m-badge m-badge--success m-badge--wide">{{ $list->status_pinjam }}</label>
                                @elseif($list->status_pinjam == 'Reject')
                                <label class="m-badge m-badge--danger m-badge--wide">{{ $list->status_pinjam }}</label>
                                @elseif($list->status_pinjam == 'Dikembalikan')
                                <label class="m-badge m-badge--primary m-badge--wide">{{ $list->status_pinjam }}</label>
                                @endif
                            </td>
                            <td class="tanggal_pinjam">{{ $list->created_at->format('l, d M Y') }}</td>
                            <td class="tanggal_kembali">{{ $list->tanggal_kembali }}</td>
                            <td align="center">
                                @if ($list->status_pinjam == 'Aprove')
                                    <a data-toggle="modal" data-target="#kembali-{{ $list->id }}" style="text-decoration: none; margin-right:10px">
                                        <button class="bg-primary" style="border:none; border-radius: 5px;"><i class="fa fa-check text-light" aria-hidden="true"></i></button>
                                    </a>
                                @elseif($list->status_pinjam == 'Request')
                                    <a data-id="{{ $list->id }}" class="deleting" style="text-decoration: none; margin-right:10px">
                                        <button class="bg-danger text-light px-2 py-1" style="border:none; border-radius: 5px;"><i class="fa fa-trash text-light" aria-hidden="true"></i></button>
                                    </a>
                                @elseif($list->status_pinjam == 'Reject')
                                    <a data-id="{{ $list->id }}" class="deleting" style="text-decoration: none; margin-right:10px">
                                        <button class="bg-danger text-light px-2 py-1" style="border:none; border-radius: 5px;"><i class="fa fa-trash text-light" aria-hidden="true"></i></button>
                                    </a>
                                @elseif($list->status_pinjam == 'Dikembalikan')
                                    <a data-id="{{ $list->id }}" class="deleting" style="text-decoration: none; margin-right:10px">
                                        <button class="bg-danger text-light px-2 py-1" style="border:none; border-radius: 5px;"><i class="fa fa-trash text-light" aria-hidden="true"></i></button>
                                    </a>
                                @endif
                            </td>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>

            {{-- REQUEST PEMINJAMAN --}}
            @foreach ($pinjam as $data)
                <div class="modal fade" id="kembali-{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                    Kembalikan Akun Zoom
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" arial-label="Close">
                                    <span aria-hidden="true">
                                        &times;
                                    </span>
                                </button>
                            </div>
                            <form action="{{ url('/post_req/'.$data->id) }}" method="POST">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">
                                            ID Akun
                                        </label>
                                        <input disabled type="text" value="{{ $data->zoom->id }}" name="id_akun" class="form-control" id="e_id_akun">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">
                                            Nama Akun
                                        </label>
                                        <input disabled type="text" value="{{ $data->zoom->nama_akun }}" name="nama_akun" class="form-control" id="e_nama_akun">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">
                                            Nama Peminjam
                                        </label>
                                        <input disabled type="text" value="{{ $data->nama_peminjam }}" name="nama_peminjam" class="form-control" id="e_nama_peminjam">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">
                                            Durasi
                                        </label>
                                        <input disabled type="text" value="{{ $data->durasi }} " name="durasi" class="form-control" id="e_durasi">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">
                                            Tanggal Peminjaman
                                        </label>
                                        <input disabled type="text" value="{{ $data->created_at->format('l, d M Y') }}" name="created_at" class="form-control" id="created_at">
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">
                                            Tanggal Kembali
                                        </label>
                                        <input type="date" value="" name="tanggal_kembali" class="form-control m-input" id="e_tanggal_kembali">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">
                                            Status Peminjaman
                                        </label>
                                        <input type="text" value="Dikembalikan" name="status_pinjam" class="form-control" id="e_status_pinjam">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                                        Close
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Kembalikan
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
            {{-- REQUEST PEMINJAMAN --}}

@endsection