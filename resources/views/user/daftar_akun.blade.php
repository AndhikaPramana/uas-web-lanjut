@extends('layouts.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Daftar Akun Zoom yang Tersedia</h1>
    
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <button href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" style="margin-left: 930px">
            Tambah Data
        </button>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID Zoom</th>
                        <th>Nama Akun</th>
                        <th>Kapasitas</th>
                        <th>Nama Peminjam</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td class="text-center">
                            <a href="#" class="btn btn-primary btn-sm">
                                <i class="fas fa-pen-square"></i>
                            </a>
                            &ensp;
                            <form action="#" method="POST" onsubmit="return confirm('Data ini akan dihapus apakah anda yakin ? ')" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger btn-sm">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection