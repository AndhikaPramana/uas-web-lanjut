@extends('layouts.app')

<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            select : true
        });       
    });
</script>
<script>
    $('.hapus').click(function () {
        var hapus = $(this).attr('data-id')
        swal({
            title: "Anda yakin ingin menghapus data ini?",
            text: "Jika iya, data ini akan terhapus secara permanent!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            window.location = "/hapus/"+hapus+""
            swal("Data anda berhasil terhapus secara permanent!", {
            icon: "success",
            });
        } else {
            swal("Data tidak jadi di hapus!");
        }
    });
});
</script>

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Management Jadwal</h1>

<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>NO</th>                        
                        <th>Nama Akun</th>
                        <th>Nama Peminjam</th>
                        <th>Status Peminjaman</th>
                        <th>Tanggal Peminjaman</th>

                        <th>Action</th>
                    </tr>
                </thead>
                    @foreach ($detail as $list)
                        <tbody>
                            <td>{{ $loop->iteration }}</td>
                            <td class="name">{{ $list->zoom->nama_akun }}</td>
                            <td class="nama_peminjam">{{ $list->nama_peminjam }}</td>
                            <td class="status" align="center">
                                @if ($list->status_pinjam == 'Aprove')
                                <label class="m-badge m-badge--success m-badge--wide">{{ $list->status_pinjam }}</label>
                                @elseif ($list->status_pinjam != 'Aprove')
                                <label class="m-badge m-badge--primary m-badge--wide">{{ $list->status_pinjam }}</label>
                                @endif
                            </td>
                            <td class="tanggal_pinjam">{{ $list->created_at->format('l, d M Y') }}</td>
                            <td align="center">
                                @if ($list->status_pinjam == 'Aprove')
                                <a style="text-decoration: none; margin-right:10px" data-toggle="modal" data-target="#detail-{{ $list->id }}">
                                    <button type="button" class="bg-primary" style="border:none; border-radius: 5px;"><i class="fa fa-eye text-light" aria-hidden="true"></i></button>
                                </a>
                                <a style="text-decoration: none; margin-right:10px" data-toggle="lihat" data-target="#melihat-{{ $list->id }}">
                                    <button type="button" class="bg-secondary" style="border:none; border-radius: 5px;"><i class="fa fa-ban text-light" aria-hidden="true"></i></button>
                                </a>
                                @elseif ($list->status_pinjam != 'Aprove')
                                <a style="text-decoration: none; margin-right:10px" data-toggle="modal" data-target="#detail-{{ $list->id }}">
                                    <button type="button" class="bg-primary" style="border:none; border-radius: 5px;"><i class="fa fa-eye text-light" aria-hidden="true"></i></button>
                                </a>
                                @endif
                            </td>
                        </tbody>
                        @endforeach
            </table>
        </div>
    </div>
</div>
@endsection