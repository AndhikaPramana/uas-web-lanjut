@extends('layouts.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Management Akun Zoom</h1>
    
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('createzoom') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            Tambah Data
        </a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Akun</th>
                        <th>Kapasitas</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dataZoom as $items)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$items->nama_akun}}</td>
                        <td>{{$items->kapasitas}}</td>
                        <td>
                            @if ($items->status_aktif == 'Aktif')
                                <button class="btn m-btn--pill btn-primary btn-sm m-btn m-btn--custom">
                                    {{ $items->status_aktif }}
                                </button>
                            @elseif($items->status_aktif == 'Tidak Aktif')
                                <button class="btn m-btn--pill btn-danger btn-sm m-btn m-btn--custom">
                                    {{ $items->status_aktif }}
                                </button>
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="/editzoom/{{ $items->id }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-pen-square"></i>
                            </a>
                            &ensp;
                            <a href="/deletezoom/{{ $items->id }}" class="btn btn-danger btn-sm far fa-trash-alt"></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection