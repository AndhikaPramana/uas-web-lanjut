@extends('layouts.app')

@section('content')
<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Ajukan</strong> Peminjaman Akun Zoom
      </div>
      <div class="card-body card-block">
    <form action="{{ route('insertzoom') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
            {{ csrf_field() }} 
            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label"><strong>Nama Akun</strong></label></div>
                <div class="col-12 col-md-9"><input type="text" id="text-input" name="nama_akun" placeholder="Masukkan Nama Akun Zoom Anda" class="form-control @error('kapasitas') is-invalid @enderror">
                    @error('kapasitas')
                    <div class="invalid-feedback">
                    {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>

            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label"><strong>Kapasitas</strong></label></div>
                <div class="col-12 col-md-9">
                    <input type="text" id="text-input" name="kapasitas" placeholder="Masukkan Kapasitas Room Zoom" class="form-control @error('kapasitas') is-invalid @enderror">
                    @error('kapasitas')
                    <div class="invalid-feedback">
                    {{ $message }}
                    </div>
                    @enderror
                </div>   
            </div>
            
            {{-- <div class="row form-group">
                <div class="col col-md-3"><strong>Status</strong></div>
                <fieldset class=" col-12 col-md-9 form-label-group  form-group  position-relative">
                    <select class="form-control"  name="status_aktif" required="pilih">
                        <option>Pilih</option>
                        <option>Aktif</option>
                        <option>Tidak Aktif</option>
                    </select>
                </fieldset>
            </div> --}}

            <div class="row"><div class="form-group col-12 col-md-9">
                <button type="submit" class="btn btn-success" style="border-radius: 5px; margin-right:1rem"><i class="fas fa-check-circle"></i>Ajukan</button>
                <a href="/listzoom" type="submit" class="btn btn-danger" style="border-radius: 5px"><i class="far fa-window-close"></i></i> Batal </a></div>
            </div>
        </div>
    </form>
</div>
@endsection