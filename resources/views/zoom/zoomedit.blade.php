@extends('layouts.app')

@section('content')
<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Ajukan</strong> Peminjaman Akun Zoom
      </div>
      <div class="card-body card-block">
    <form action="{{ route('updatezoom', ['id' => $dataZoom->id]) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
            {{ csrf_field() }} 
            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label"><strong>Nama Akun</strong></label></div>
                <div class="col-12 col-md-9"><input type="text" required id="text-input" name="nama_akun" value="{{ $dataZoom->nama_akun }}" class="form-control"></div>
            </div>

            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label"><strong>Kapasitas</strong></label></div>
                <div class="col-12 col-md-9"><input type="text" required id="text-input" name="kapasitas" value="{{ $dataZoom->kapasitas }}" class="form-control"></div>
            </div>
            
            <div class="row form-group">
                <div class="col col-md-3"><strong>Status</strong></div>
                <fieldset class=" col-12 col-md-9 form-label-group  form-group  position-relative">
                    <select class="form-control" name="status_aktif" required="" value="{{ $dataZoom->status_aktif }}">
                        <option value="aktif">Aktif</option>
                        <option value="tidak aktif">Tidak Aktif</option>
                    </select>
                </fieldset>
            </div>

            <div class="row"><div class="form-group col-12 col-md-9">
                <button type="submit" class="btn btn-success" style="border-radius: 5px; margin-right:1rem"><i class="fas fa-check-circle"></i>Simpan</button>
                <a href="{{ route('zoomlist') }}" type="submit" class="btn btn-danger" style="border-radius: 5px"><i class="far fa-window-close"></i></i> Batal </a></div>
            </div>
        </div>
    </form>
</div>
@endsection