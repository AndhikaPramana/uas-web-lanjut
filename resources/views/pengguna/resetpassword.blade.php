
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Vuesax admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, Vuesax admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="PIXINVENT">
  <title>Login Page</title>
  <link rel="apple-touch-icon" href="{{asset('images/ico/apple-icon-120.png')}}">
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/ico/favicon.ico')}}">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

  <!-- BEGIN: Vendor CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/vendors.min.css')}}">
  <!-- END: Vendor CSS-->

  <!-- BEGIN: Theme CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-extended.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/colors.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/components.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/themes/dark-layout.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/themes/semi-dark-layout.css')}}">

  <!-- BEGIN: Page CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('css/core/menu/menu-types/vertical-menu.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/core/colors/palette-gradient.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/pages/authentication.css')}}">
  <!-- END: Page CSS-->

  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
  <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="semi-dark-layout">
  <!-- BEGIN: Content-->
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="row flexbox-container">
          <div class="col-xl-8 col-11 d-flex justify-content-center">
            <div class="card bg-authentication rounded-0 mb-0">
              <div class="row m-0">
                {{-- <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                  <img src="{{asset('images/pages/login.png')}}" alt="branding logo">
                </div> --}}
                <div class="col-lg-6 col-12 p-0">
                  <div class="card rounded-0 mb-0 px-2">
                    <div class="card-header pb-1">
                      <div class="card-title">
                        <h4 class="mb-0">Reset Password</h4>
                      </div>
                    </div>
                    <p class="px-2">Please Enter for you reset password.</p>
                    @if (session()->has('errorresetlink'))
                    <div class="alert alert-danger" role="alert" style="font-size: 12px">
                      {!! session('errorresetlink') !!}
                    </div>
                    @elseif (session()->has('errorresetps'))
                    <div class="alert alert-danger" role="alert" style="font-size: 12px">
                      {!! session('errorresetps') !!}
                    </div>
                    @endif
                    <div class="card-content">
                      <div class="card-body pt-1">

                        <form action="{{ route('postResetPassword',$reset_code) }}" class="user" method="POST">
                        @csrf
                          <fieldset class="form-label-group form-group position-relative has-icon-left">
                            <input type="text" name="email" class="form-control" id="email" placeholder="Email" required>
                            <div class="form-control-position">
                              <i class="feather icon-mail"></i>
                            </div>
                            <label for="email">Email</label>
                          </fieldset>

                          <fieldset class="form-label-group position-relative has-icon-left">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                            <div class="form-control-position">
                              <i class="feather icon-lock"></i>
                            </div>
                            <label for="password">Password</label>
                          </fieldset>

                          <fieldset class="form-label-group position-relative has-icon-left">
                          <input id="confirm_password" type="password" onkeyup="check()" class="form-control" name="confirm_password" placeholder="Confirm Password" required>
                                <span id='message'></span>
                            <div class="form-control-position">
                              <i class="feather icon-check"></i>
                            </div>
                            <label for="password">Confirm Password</label>
                          </fieldset>
                          <!-- <a href="auth-register.html" class="btn btn-outline-primary float-left btn-inline">Register</a> -->
                          <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
  <!-- END: Content-->


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/sb-admin/vendor/jquery/jquery.min.js"></script>
  <script src="vendor/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="vendor/sb-admin/js/sb-admin-2.min.js"></script>

  <!-- BEGIN: Page JS-->
  <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>