<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjam', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('akun_id')->unsigned();
            $table->string('nama_akun')->nullable();
            $table->string('nama_peminjam');
            $table->string('nama_kegiatan');
            $table->string('keterangan', 500);
            $table->time('durasi');
            $table->string('status_pinjam')->nullable();
            $table->timestamp('tanggal_kembali')->nullable();
            $table->timestamps();
        });
        
        Schema::table('pinjam', function (Blueprint $table) {
            $table->foreign('akun_id')->references('id')->on('zoom')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjam');
    }
}
